#include <Verso/DepthIo/TiltStateRawKinect.hpp>

namespace Verso {


const double TiltStateRawKinect::COUNTS_PER_G = 819.0f;
const double TiltStateRawKinect::GRAVITY_ACCELERATION = 9.80665f;


TiltStateRawKinect::TiltStateRawKinect(int16_t accelerometerX, int16_t accelerometerY, int16_t accelerometerZ) :
	accelerometerX(accelerometerX),
	accelerometerY(accelerometerY),
	accelerometerZ(accelerometerZ)
{
}


void TiltStateRawKinect::set(int16_t accelerometerX, int16_t accelerometerY, int16_t accelerometerZ)
{
	this->accelerometerX = accelerometerX;
	this->accelerometerY = accelerometerY;
	this->accelerometerZ = accelerometerZ;
}


void TiltStateRawKinect::getGravityAdjusted(double& x, double& y, double& z)
{
	x = (double)accelerometerX / COUNTS_PER_G * GRAVITY_ACCELERATION;
	y = (double)accelerometerY / COUNTS_PER_G * GRAVITY_ACCELERATION;
	z = (double)accelerometerZ / COUNTS_PER_G * GRAVITY_ACCELERATION;
}


bool TiltStateRawKinect::equals(const TiltStateRawKinect& obj) const
{
	if (accelerometerX != obj.accelerometerX)
		return false;

	if (accelerometerY != obj.accelerometerY)
		return false;

	if (accelerometerZ != obj.accelerometerZ)
		return false;

	return true;
}


} // End of namespace

