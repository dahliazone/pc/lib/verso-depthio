#include <Verso/DepthIo/KinectPlaintextFile.hpp>
#include <fstream>

namespace Verso {


/////////////////////////////////////////////////////////////////////////
// Load kinect plaintext file
/////////////////////////////////////////////////////////////////////////

void processLoadedRow(FrameAnimation& anim, const UString& row)
{
	if (row.size() == 0) // skip empty lines
		return;
	if (row.charAt(0) == '#' || row.charAt(0) == '/') // skip comments
		return;

	//VERSO_LOG_INFO_VARIABLE("depthio/test", "Read row", "|\""<<row<<"\"|");

	// Read timestamp
	if (row.beginsWith("==NEWFRAME==") == true) {
		size_t newFrameLen = 12;
		UString temp = row.substring(newFrameLen, row.size() - newFrameLen - 2);
		uint32_t timestamp = temp.toValue<uint32_t>();
		VERSO_LOG_INFO("depthio/test", "Found timestamp "<<timestamp<<". Creating new frame...");
		anim.createNewFrame(DioDataType::Array_Xyz, timestamp);
	}

	// Read point cloud data
	else {
		size_t lastSplit = 0;
		float coords[3];
		size_t coordIndex = 0;
		for (size_t i=0; i<row.size(); ++i) {
			if (row.charAt(i) == ';') {
				UString temp = row.substring(lastSplit, i - lastSplit);
				float value = temp.toValue<float>();
				lastSplit = i + 1;
				//VERSO_LOG_INFO("depthio", "; found value: "<<value);
				VERSO_ASSERT("verso-depthio", coordIndex < 3 && "INVALID INPUT FILE. MORE THAN 3 COORDINATES PER POINT");
				coords[coordIndex] = value;

				anim.getNewestFrame().addDataXyz(coords[0], coords[1], coords[2]);
				coordIndex = 0;
			}
			else if (row.charAt(i) == ',') {
				UString temp = row.substring(lastSplit, i - lastSplit);
				float value = temp.toValue<float>();
				lastSplit = i + 1;
				//VERSO_LOG_INFO("depthio", ", found value: "<<value);
				VERSO_ASSERT("verso-depthio", coordIndex < 3 && "INVALID INPUT FILE. MORE THAN 3 COORDINATES PER POINT");
				coords[coordIndex] = value;
				++coordIndex;
			}
		}
	}
}


bool loadKinectPlaintextFile(FrameAnimation& anim, const UString& fileName)
{
	VERSO_LOG_INFO("depthio", "Loading file \""<<fileName<<"\"...");
	std::ifstream file(fileName.c_str(), std::ios::in);
	if (!file)
		return false;

	std::string buffer;
	while (std::getline(file, buffer)) {
		processLoadedRow(anim, UString(buffer));
		buffer = "";
	}
	file.close();
	return true;
}


/////////////////////////////////////////////////////////////////////////
// Save kinect plaintext file
/////////////////////////////////////////////////////////////////////////

bool saveKinectPlaintextFile(const FrameAnimation& anim, const UString& fileName)
{
	(void)anim; (void)fileName;
	VERSO_FAIL("verso-depthio");
	return false;
}


} // End of namespace

