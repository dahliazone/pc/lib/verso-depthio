// Tested 100% 2016-10-03 -codise

#include <Verso/DepthIo/DioPackFormat.hpp>
#include <gtest/gtest.h>

namespace {

using namespace Verso;


/**
 * \brief Test constructor.
 */
TEST(DioPackFormat, constructor)
{
	DioPackFormat type1 = DioPackFormat::Unpacked;

	EXPECT_EQ(DioPackFormat::Unpacked, type1);
	EXPECT_EQ(type1, DioPackFormat::Unpacked);
}


/**
 * \brief Test UString dioPackFormatToString(DioPackFormat dioPackFormat).
 */
TEST(DioPackFormat, dioPackFormatToString)
{
	DioPackFormat valid1 = DioPackFormat::Unpacked;
	EXPECT_STRCASEEQ(dioPackFormatToString(valid1).c_str(), "Unpacked");

	DioPackFormat valid2 = DioPackFormat::Packed_Snappy;
	EXPECT_STRCASEEQ(dioPackFormatToString(valid2).c_str(), "Packed with Snappy");

	DioPackFormat invalid = (DioPackFormat)128;
	EXPECT_STRCASEEQ(dioPackFormatToString(invalid).c_str(), "UNKNOWN");
}


/**
 * \brief Test std::vector<DioPackFormat> getAllDioPackFormats().
 */
TEST(DioPackFormat, getAllDioVariableTypes)
{
	std::vector<DioPackFormat> formats = getAllDioPackFormats();

	EXPECT_EQ(formats.size(), 2);
	EXPECT_TRUE(std::find(formats.begin(), formats.end(), DioPackFormat::Unpacked) != formats.end());
	EXPECT_TRUE(std::find(formats.begin(), formats.end(), DioPackFormat::Packed_Snappy) != formats.end());
}


/**
 * \brief Test std::ostream& operator << (std::ostream& os, const DioPackFormat& dioPackFormat).
 */
TEST(DioPackFormat, operator_lessthanlessthan)
{
	std::ostringstream oss;
	oss << DioPackFormat::Unpacked;
	EXPECT_STRCASEEQ(oss.str().c_str(), "Unpacked");
	oss.clear();
	oss.str("");

	oss << DioPackFormat::Packed_Snappy;
	EXPECT_STRCASEEQ(oss.str().c_str(), "Packed with Snappy");
	oss.clear();
	oss.str("");

	oss << DioPackFormat::Unset;
	EXPECT_STRCASEEQ(oss.str().c_str(), "Unset");
	oss.clear();
	oss.str("");

	oss << (DioPackFormat)129;
	EXPECT_STRCASEEQ(oss.str().c_str(), "UNKNOWN");
}


} // End namespace nameless namespace

