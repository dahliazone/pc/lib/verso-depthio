// Tested 100% 2016-10-03 -codise

#include <Verso/DepthIo/DioVariableType.hpp>
#include <gtest/gtest.h>

namespace {

using namespace Verso;


/**
 * \brief Test constructor.
 */
TEST(DioVariableType, constructor)
{
	DioVariableType type1 = DioVariableType::Doudble64;

	EXPECT_EQ(DioVariableType::Doudble64, type1);
	EXPECT_EQ(type1, DioVariableType::Doudble64);
}


/**
 * \brief Test UString dioVariableTypeToString(DioVariableType dioVariableType).
 */
TEST(DioVariableType, dioVariableTypeToString)
{
	DioVariableType valid1 = DioVariableType::Sint8;
	EXPECT_STRCASEEQ(dioVariableTypeToString(valid1).c_str(), "Sint8");

	DioVariableType valid2 = DioVariableType::Uint16;
	EXPECT_STRCASEEQ(dioVariableTypeToString(valid2).c_str(), "Uint16");

	DioVariableType invalid = (DioVariableType)128;
	EXPECT_STRCASEEQ(dioVariableTypeToString(invalid).c_str(), "UNKNOWN");
}


/**
 * \brief Test std::vector<DioVariableType> getAllDioVariableTypes().
 */
TEST(DioVariableType, getAllDioVariableTypes)
{
	std::vector<DioVariableType> types = getAllDioVariableTypes();

	EXPECT_EQ(types.size(), 8);
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioVariableType::Uint8) != types.end());
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioVariableType::Uint16) != types.end());
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioVariableType::Uint32) != types.end());
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioVariableType::Sint8) != types.end());
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioVariableType::Sint16) != types.end());
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioVariableType::Sint32) != types.end());
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioVariableType::Float32) != types.end());
	EXPECT_TRUE(std::find(types.begin(), types.end(), DioVariableType::Doudble64) != types.end());
}


/**
 * \brief Test std::ostream& operator << (std::ostream& os, const DioVariableType& dioVariableType).
 */
TEST(DioVariableType, operator_lessthanlessthan)
{
	std::ostringstream oss;
	oss << DioVariableType::Sint8;
	EXPECT_STRCASEEQ(oss.str().c_str(), "Sint8");
	oss.clear();
	oss.str("");

	oss << DioVariableType::Doudble64;
	EXPECT_STRCASEEQ(oss.str().c_str(), "Doudble64");
	oss.clear();
	oss.str("");

	oss << DioVariableType::Unset;
	EXPECT_STRCASEEQ(oss.str().c_str(), "Unset");
	oss.clear();
	oss.str("");

	oss << (DioVariableType)129;
	EXPECT_STRCASEEQ(oss.str().c_str(), "UNKNOWN");
}


} // End namespace nameless namespace

