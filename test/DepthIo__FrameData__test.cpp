#include <Verso/DepthIo/FrameData.hpp>
#include <gtest/gtest.h>

namespace {

using namespace Verso;


/**
 * Test constructor.
 */
TEST(FrameData, constructor)
{
	FrameData fd1(0); // index = 0, others default
	EXPECT_EQ(0, fd1.getIndex());
	EXPECT_EQ(0, fd1.getTimestamp());
	EXPECT_EQ(0.0f, fd1.getTimestampSec());
	EXPECT_EQ(0, fd1.getIndex());
	EXPECT_EQ(0, fd1.pointsXyzCount());
	EXPECT_EQ(0, fd1.pointsDepthCount());
	EXPECT_EQ(0, fd1.pointsRgbCount());
	EXPECT_EQ(0, fd1.pointsCount());

	FrameData fd2(2, DioDataType::Array_Depth | DioDataType::Array_Rgb, 3000);
	EXPECT_EQ(2, fd2.getIndex());
	EXPECT_EQ((DioDataTypes)DioDataType::Array_Depth | (DioDataTypes)DioDataType::Array_Rgb, fd2.getRequiredDioDataTypes());
	EXPECT_EQ(3000, fd2.getTimestamp());
	EXPECT_EQ(3.0f, fd2.getTimestampSec());
	EXPECT_EQ(0, fd2.pointsXyzCount());
	EXPECT_EQ(0, fd2.pointsDepthCount());
	EXPECT_EQ(0, fd2.pointsRgbCount());
	EXPECT_EQ(0, fd2.pointsCount());
}


/**
 * Test setters.
 */
TEST(FrameData, setters)
{
	FrameData fd1(0);

	fd1.setIndex(4);
	EXPECT_EQ(4, fd1.getIndex());

	fd1.setRequiredDioDataTypes(DioDataType::Array_Intensity);
	EXPECT_EQ((DioDataTypes)DioDataType::Array_Intensity, fd1.getRequiredDioDataTypes());

	EXPECT_EQ((DioDataTypes)0, fd1.getActualDioDataTypes());

	fd1.addDataXyz(0.0f, 0.0f, 0.0f);
	EXPECT_EQ((DioDataTypes)DioDataType::Array_Xyz, fd1.getActualDioDataTypes());
	EXPECT_EQ(1, fd1.pointsXyzCount());
	EXPECT_EQ(0, fd1.pointsDepthCount());
	EXPECT_EQ(0, fd1.pointsRgbCount());
	EXPECT_EQ(1, fd1.pointsCount());

	fd1.addDataDepth(0);
	EXPECT_EQ((DioDataTypes)DioDataType::Array_Xyz | (DioDataTypes)DioDataType::Array_Depth, fd1.getActualDioDataTypes());
	EXPECT_EQ(1, fd1.pointsXyzCount());
	EXPECT_EQ(1, fd1.pointsDepthCount());
	EXPECT_EQ(0, fd1.pointsRgbCount());
	EXPECT_EQ(1, fd1.pointsCount());

	fd1.addDataRgb(0, 0, 0);
	EXPECT_EQ((DioDataTypes)DioDataType::Array_Xyz | (DioDataTypes)DioDataType::Array_Depth | (DioDataTypes)DioDataType::Array_Rgb,
			  fd1.getActualDioDataTypes());
	EXPECT_EQ(1, fd1.pointsXyzCount());
	EXPECT_EQ(1, fd1.pointsDepthCount());
	EXPECT_EQ(1, fd1.pointsRgbCount());
	EXPECT_EQ(1, fd1.pointsCount());

	fd1.clearDataXyz();
	EXPECT_EQ((DioDataTypes)DioDataType::Array_Depth | (DioDataTypes)DioDataType::Array_Rgb, fd1.getActualDioDataTypes());
	EXPECT_EQ(0, fd1.pointsXyzCount());
	EXPECT_EQ(1, fd1.pointsDepthCount());
	EXPECT_EQ(1, fd1.pointsRgbCount());
	EXPECT_EQ(1, fd1.pointsCount());

	fd1.clearDataRgb();
	EXPECT_EQ((DioDataTypes)DioDataType::Array_Depth, fd1.getActualDioDataTypes());
	EXPECT_EQ(0, fd1.pointsXyzCount());
	EXPECT_EQ(1, fd1.pointsDepthCount());
	EXPECT_EQ(0, fd1.pointsRgbCount());
	EXPECT_EQ(1, fd1.pointsCount());

	fd1.clearDataDepth();
	EXPECT_EQ((DioDataTypes)0, fd1.getActualDioDataTypes());
	EXPECT_EQ(0, fd1.pointsXyzCount());
	EXPECT_EQ(0, fd1.pointsDepthCount());
	EXPECT_EQ(0, fd1.pointsRgbCount());
	EXPECT_EQ(0, fd1.pointsCount());

	fd1.setTimestamp(5000);
	EXPECT_EQ(5000, fd1.getTimestamp());
	EXPECT_EQ(5.0f, fd1.getTimestampSec());


	FrameData fd2(0);
	EXPECT_EQ(0, fd2.pointsXyzCount());
	EXPECT_EQ(0, fd2.pointsDepthCount());
	EXPECT_EQ(0, fd2.pointsRgbCount());
	EXPECT_EQ(0, fd2.pointsCount());

	size_t pointCount = 2;
	float* extPtrDataXyz = new float[pointCount];
	uint16_t* extPtrDataDepth = new uint16_t[pointCount];
	uint8_t* extPtrDataRgb = new uint8_t[pointCount];

	fd2.setExternalDataXyz(extPtrDataXyz, pointCount);
	EXPECT_EQ(pointCount, fd2.pointsXyzCount());
	EXPECT_EQ(0, fd2.pointsDepthCount());
	EXPECT_EQ(0, fd2.pointsRgbCount());
	EXPECT_EQ(pointCount, fd2.pointsCount());

	fd2.setExternalDataDepth(extPtrDataDepth, pointCount);
	EXPECT_EQ(pointCount, fd2.pointsXyzCount());
	EXPECT_EQ(pointCount, fd2.pointsDepthCount());
	EXPECT_EQ(0, fd2.pointsRgbCount());
	EXPECT_EQ(pointCount, fd2.pointsCount());

	fd2.setExternalDataRgb(extPtrDataRgb, pointCount);
	EXPECT_EQ(pointCount, fd2.pointsXyzCount());
	EXPECT_EQ(pointCount, fd2.pointsDepthCount());
	EXPECT_EQ(pointCount, fd2.pointsRgbCount());
	EXPECT_EQ(pointCount, fd2.pointsCount());

	EXPECT_EQ(extPtrDataXyz, fd2.getDataXyz());
	EXPECT_EQ(extPtrDataDepth, fd2.getDataDepth());
	EXPECT_EQ(extPtrDataRgb, fd2.getDataRgb());

	fd2.clearDataXyz();
	EXPECT_EQ(nullptr, fd2.getDataXyz());
	EXPECT_EQ(extPtrDataDepth, fd2.getDataDepth());
	EXPECT_EQ(extPtrDataRgb, fd2.getDataRgb());

	fd2.clearDataDepth();
	EXPECT_EQ(nullptr, fd2.getDataXyz());
	EXPECT_EQ(nullptr, fd2.getDataDepth());
	EXPECT_EQ(extPtrDataRgb, fd2.getDataRgb());

	fd2.clearDataRgb();
	EXPECT_EQ(nullptr, fd2.getDataXyz());
	EXPECT_EQ(nullptr, fd2.getDataDepth());
	EXPECT_EQ(nullptr, fd2.getDataRgb());

	delete[] extPtrDataXyz;
	delete[] extPtrDataDepth;
	delete[] extPtrDataRgb;
}


/**
 * Test CheckIfDataIsValid().
 */
TEST(FrameData, CheckIfDataIsValid)
{
	FrameData fd1(0);
	EXPECT_STRCASEEQ("", fd1.getErrorMsg().c_str());
	fd1.clearErrorMsg();

	EXPECT_FALSE(fd1.checkIfDataIsValid());
	EXPECT_STRCASENE("", fd1.getErrorMsg().c_str());
	fd1.clearErrorMsg();

	fd1.setRequiredDioDataTypes((DioDataTypes)DioDataType::Array_Xyz |
								(DioDataTypes)DioDataType::Array_Depth |
								(DioDataTypes)DioDataType::Array_Rgb);
	EXPECT_FALSE(fd1.checkIfDataIsValid());
	EXPECT_STRCASENE("", fd1.getErrorMsg().c_str());
	fd1.clearErrorMsg();

	fd1.addDataDepth(666);
	EXPECT_FALSE(fd1.checkIfDataIsValid());
	EXPECT_STRCASENE("", fd1.getErrorMsg().c_str());
	fd1.clearErrorMsg();

	fd1.addDataRgb(1, 2, 3);
	EXPECT_FALSE(fd1.checkIfDataIsValid());
	EXPECT_STRCASENE("", fd1.getErrorMsg().c_str());
	fd1.clearErrorMsg();

	fd1.addDataXyz(4, 5, 6);
	EXPECT_TRUE(fd1.checkIfDataIsValid());
	EXPECT_STRCASEEQ("", fd1.getErrorMsg().c_str());
	fd1.clearErrorMsg();

	fd1.addDataDepth(667);
	EXPECT_FALSE(fd1.checkIfDataIsValid());
	EXPECT_STRCASENE("", fd1.getErrorMsg().c_str());
	fd1.clearErrorMsg();

	fd1.addDataXyz(4, 5, 6);
	EXPECT_FALSE(fd1.checkIfDataIsValid());
	EXPECT_STRCASENE("", fd1.getErrorMsg().c_str());
	fd1.clearErrorMsg();

	fd1.addDataRgb(1, 2, 3);
	EXPECT_TRUE(fd1.checkIfDataIsValid());
	EXPECT_STRCASEEQ("", fd1.getErrorMsg().c_str());
	fd1.clearErrorMsg();
}


} // End namespace nameless namespace

