#ifndef VERSO_DEPTHIO_DIOPACKFORMAT_HPP
#define VERSO_DEPTHIO_DIOPACKFORMAT_HPP

#include <Verso/System/UString.hpp>
#include <cstdint>

namespace Verso {


enum class DioPackFormat : std::uint8_t
{
	Unpacked = 0,
	Packed_Snappy = 1,
	Unset = 255
};


inline UString dioPackFormatToString(DioPackFormat dioPackFormat)
{
	if (dioPackFormat == DioPackFormat::Unpacked) {
		return "Unpacked";
	}
	else if (dioPackFormat == DioPackFormat::Packed_Snappy) {
		return "Packed with Snappy";
	}
	else if (dioPackFormat == DioPackFormat::Unset) {
		return "Unset";
	}
	else {
		return "UNKNOWN";
	}
}


inline std::vector<DioPackFormat> getAllDioPackFormats()
{
	std::vector<DioPackFormat> dioPackFormats;
	dioPackFormats.push_back(DioPackFormat::Unpacked);
	dioPackFormats.push_back(DioPackFormat::Packed_Snappy);
	return dioPackFormats;
}


inline std::ostream& operator << (std::ostream& ost, const DioPackFormat& dioPackFormat)
{
	return ost << dioPackFormatToString(dioPackFormat);
}


} // End of namespace

#endif // End header guard

