#ifndef VERSO_DEPTHIO_FRAMEANIMATION_HPP
#define VERSO_DEPTHIO_FRAMEANIMATION_HPP

#include <Verso/DepthIo/FrameData.hpp>
#include <Verso/System/UString.hpp>
#include <Verso/System/Logger.hpp>
#include <stdint.h>
#include <vector>

namespace Verso {


struct VERSO_DEPTHIO_API FrameAnimation
{
	// @width: Either: maximum amount of points per frame (unordered set) or zero if values is unknown (unordered set) or number of points in a row each frame (ordered dataset).
	// @height: 1 (unordered set) or number of rows each frame (ordered dataset).
	FrameAnimation(uint32_t width_, uint32_t height_) :
		width(width_),
		height(height_),
		frames()
	{
	}


	void createNewFrame(uint32_t pointCloudDataTypes, uint32_t timestamp)
	{
		frames.push_back(FrameData(frames.size(), pointCloudDataTypes, timestamp));
	}


	FrameData& getFrame(size_t index)
	{
		return frames[index];
	}


	const FrameData& getFrame(size_t index) const
	{
		return frames[index];
	}


	FrameData& getNewestFrame()
	{
		return frames[frames.size() - 1];
	}


	const FrameData& getNewestFrame() const
	{
		return frames[frames.size() - 1];
	}


	void addFrame(FrameData& frame)
	{
		frames.push_back(frame);
		updateWidthIfNeeded();
	}


	size_t lengthInFrames() const
	{
		return frames.size();
	}


	uint32_t lengthInMillisec() const
	{
		if (frames.size() == 0) {
			return 0;
		}

		uint32_t len = 0;
		for (size_t i = 1; i < frames.size(); ++i) {
			len += frames[i].getTimestamp() - frames[i-1].getTimestamp();
		}
		return len;
	}


	float lengthInSec() const
	{
		return static_cast<float>(lengthInMillisec() / 1000.0);
	}


	void updateWidthIfNeeded()
	{
		if (height == 0 || height == 1) {
			width = pointsInLargestFrame();
			height = 1;
		}
	}


	size_t pointsInLargestFrame() const
	{
		size_t largest = 0;
		for (size_t i = 0; i < frames.size(); ++i) {
			size_t amount = frames[i].pointsXyzCount();
			if (largest < amount) {
				largest = amount;
			}
		}
		return largest;
	}


	size_t pointsInTotal() const
	{
		size_t amount = 0;
		for (size_t i = 0; i < frames.size(); ++i) {
			amount += frames[i].pointsXyzCount();
		}
		return amount;
	}


	float avgPointsPerFrame() const
	{
		return static_cast<float>(pointsInTotal()) / static_cast<float>(lengthInFrames());
	}


	bool checkIfDataIsValid()
	{
		for (size_t i = 0; i < frames.size(); ++i) {
			if (frames[i].checkIfDataIsValid() == false) {
				VERSO_LOG_ERR("verso-depthio", "Invalid data in frame "<<i);
				return false;
			}
		}
		return true;
	}


	uint32_t width;
	uint32_t height;
	std::vector<FrameData> frames;


	bool equals(const FrameAnimation& anim) const
	{
		if (width != anim.width) {
			return false;
		}
		if (height != anim.height) {
			return false;
		}
		if (frames.size() != anim.frames.size()) {
			return false;
		}
		for (size_t i=0; i<frames.size(); ++i) {
			if (frames[i].equals(anim.frames[i]) == false) {
				return false;
			}
		}
		return true;
	}


	void print() const
	{
		for (size_t i = 0; i < frames.size(); ++i) {
			VERSO_LOG_INFO_VARIABLE("verso-depthio", "frames["<<i<<"]", frames[i].toString());
		}
	}
};


} // End of namespace

#endif // End header guard

