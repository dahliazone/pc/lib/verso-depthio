
#ifndef VERSO_DEPTHIO_DEPTHIOBINARY_HPP
#define VERSO_DEPTHIO_DEPTHIOBINARY_HPP

#include <Verso/DepthIo/FrameAnimation.hpp>
#include <Verso/DepthIo/DioPackFormat.hpp>
#include <Verso/DepthIo/DioChunkType.hpp>
#include <Verso/DepthIo/DioHeaderFlagBit.hpp>
#include <fstream>
#include <stdint.h>

namespace Verso {


VERSO_DEPTHIO_API bool loadDioFile(FrameAnimation& anim, const UString& fileName);
VERSO_DEPTHIO_API bool saveDioFile(FrameAnimation& anim, const UString& fileName, DioPackFormat pack);



struct VERSO_DEPTHIO_API DioHeader
{
	uint8_t identification[8]; // \211 D i o \r \n \032 \n
	uint8_t version; // Currently 0
	std::uint64_t fileLength; // Total length of the file in bytes
	uint32_t width; // Either: Maximum amount of points per frame (unordered set) or number of points in a row each frame (ordered dataset)
	uint32_t height; // Either: 1 (unordered set) or number of rows each frame (ordered dataset)
	uint32_t flags; // See enum FileHeaderFlagBit for description of the different bits
	uint8_t packFormat; // Whether packed or not, see enum PackFormat.
	                         // \TODO: think about this. chunk specific?
	std::uint64_t timeCreated;
	std::uint64_t timeModified;
	uint16_t offsetToFirstFrameHeader; // offset in bytes from after this variable to header of first frame.
	                                        // E.g. zero means it's the next byte after this variable has been read.
	std::uint64_t footerPtr; // Pointer to footer section start.
};


struct VERSO_DEPTHIO_API DioChunkHeader_Minimal
{
	uint8_t chunkType; // Type of this block. allows different kinds of data in the file. see enum DioChunkType.
	uint32_t dataLength; // Chunk data length in bytes. (does not include the header)
	uint16_t offsetToData; // Offset in bytes from after this variable to start of the data.
	                            // E.g. zero means it's the next byte after this variable has been read.
};


struct VERSO_DEPTHIO_API DioChunkHeader_Frame
{
	uint8_t chunkType; // Type of this block. allows different kinds of data in the file. Value should be DioChunkType::FRAME.
	uint32_t dataLength; // Chunk data length in bytes. (does not include the header)
	uint16_t offsetToData; // Offset in bytes from after this variable to start of the data.
	                       // E.g. zero means it's the next byte after this variable has been read.
	uint32_t timestamp; // Timestamp start of the frame in milliseconds
	uint32_t pointCount; // Amount of points in the frame
	uint8_t dataTypeCount; // How many different DioDataTypes are contained in this frame
	uint8_t* dioDataTypes; // length=dataTypeCount*2, Contains variable and data types listed in the data section
	                       // e.g. DioVariableType::Float32, DioDataType::Array_Xyz, DioVariableType::Uint8, DioDataType::Array_Rgb
	                       // means that there are two arrays in the data section. First array that has 3 floats per point for X, Y and Z.
	                       // Straight after that a new array containing 3 uint8 for red, green and blue. After that next chunk or footer starts.
	                       // See enums DioVariableType and DioDataType.
	                       // May contain one extra entry when needed for padding to 32-bits.

	// Optional variables that need to be set when DioHeader.flags has bit HAS_VIEWPORT set.
	float viewpointTranslation[3]; // Viewpoint translation (tx ty tz). Default value: 0 0 0
	float viewpointQuaternion[4]; // Viewport rotation quaternion (qw qx qy qz). Default value: 1 0 0 0
};


typedef uint32_t DioCrc32;


struct VERSO_DEPTHIO_API DioFooter_ChunkMetadata
{
	uint8_t type; // Type for chunk metadata (reserved for future use, currently set to 0)
	uint16_t offsetToNext; // Offset to next chunk metadata
	uint8_t chunkType; // Type of this block. allows different kinds of data in the file. see enum DioChunkType.
	std::uint64_t chunkHeaderPtr; // Absolute pointer (in bytes) to start of chunk header from start of file.
	uint32_t dataLength; // Chunk data length in bytes
	uint32_t timestamp; // Optional timestamp for frame chunks. Set to zero if chunk has no timestamp.
};


struct VERSO_DEPTHIO_API DioFooter
{
	uint32_t frameCount; // amount of frames in the animation
	uint32_t lenghtMilliseconds; // length of the whole animation in milliseconds
	uint32_t chunkCount; // amount of chunks in this file
	DioFooter_ChunkMetadata* chunkMetadata; // array of DioFooter_ChunkMetadata(s). Length in bytes: chunkCount * sizeOf(DioFooter_ChunkMetadata)
};


/*
struct VERSO_DEPTHIO_API DioChunk
{
	DioChunkHeader_Minimal/DioChunkHeader_Frame chunkHeader;
	uint8_t* chunkData; // depending on the header could be xyzxyz... or xyzxyz...rgbrgb...
	DioChunkCrc32 chunkCrc0; // CRC32 of header and data
};


struct VERSO_DEPTHIO_API DioFileFormat
{
	DioHeader header;
	DioCrc32 headerCrc32; // CRC32 of the footer

	DioChunk* chunks;

	DioFooter footer;
	DioCrc32 footerCrc32; // CRC32 of the footer
};
*/


class VERSO_DEPTHIO_API DioFile
{
public:
	DioFile();
	virtual ~DioFile();

	bool startToSave(const UString& fileName, DioPackFormat pack, uint32_t width, uint32_t height);
	uint32_t getLastTimestamp() const;
	bool isNewFrame(uint32_t timestamp) const;
	// For real recording usage it's usually recommended to check the frame with IsNewFrame(timestamp) before saving it.
	bool saveFrame(FrameData& frame);
	bool finishSave();
	UString getErrorMsg() const;
	void clearErrorMsg();

private:
	static const uint8_t mVersion;
	UString mFileName;
	DioPackFormat mPackFormat;
	DioHeaderFlagBits mFlags;
	std::ofstream mFile;
	uint32_t mWidth;
	uint32_t mHeight;
	std::vector<DioFooter_ChunkMetadata> mChunkMetadata;
	uint32_t mLengthInFrames;
	uint32_t mLengthInMillisec;
	uint32_t mLastTimestamp;
	UString mErrorMsg;
};


inline UString DioFile::getErrorMsg() const
{
	return mErrorMsg;
}

inline void DioFile::clearErrorMsg()
{
	mErrorMsg = "";
}


} // End of namespace


#endif // End header guard

