#ifndef VERSO_DEPTHIO_DIODATATYPE_HPP
#define VERSO_DEPTHIO_DIODATATYPE_HPP

#include <Verso/DepthIo/DioVariableType.hpp>
#include <Verso/System/UString.hpp>
#include <cstdint>
#include <vector>

namespace Verso {


enum DioDataType : std::uint8_t
{
	Array_Xyz = 1, // array of floats, e.g. x,y,z, x,y,z, ...
	Array_NormalXyz_Curvature = 2, // array of normals + curvature, e.g. nx,ny,xz,c, nx,ny,nz,c, ...
	Array_Depth = 4, // array of uint8 depth values, e.g. d, d, ...
	Array_Rgb = 8, // array of uint8 rgb values, e.g. r,g,b, r,g,b, ...
	Array_Intensity = 16, // array of uint8 intensity values, e.g. r,g,b, r,g,b, ...
	Array_Segmentation = 32
	//Unset = 0 //
};

typedef std::uint8_t DioDataTypes;


inline UString dioDataTypeToString(DioDataType dioDataType)
{
	if (dioDataType == DioDataType::Array_Xyz) {
		return "Array_Xyz";
	}
	else if (dioDataType == DioDataType::Array_NormalXyz_Curvature) {
		return "Array_NormalXyz_Curvature";
	}
	else if (dioDataType == DioDataType::Array_Depth) {
		return "Array_Depth";
	}
	else if (dioDataType == DioDataType::Array_Rgb) {
		return "Array_Rgb";
	}
	else if (dioDataType == DioDataType::Array_Intensity) {
		return "Array_Intensity";
	}
	else if (dioDataType == DioDataType::Array_Segmentation) {
		return "Array_Segmentation";
	}
	else if (dioDataType == 0) {
		return "Unset";
	}
	else {
		return "UNKNOWN";
	}
}


inline DioVariableType dioDataTypesDioVariableType(DioDataType dioDataType)
{
	if (dioDataType == DioDataType::Array_Xyz) {
		return DioVariableType::Float32;
	}
	else if (dioDataType == DioDataType::Array_NormalXyz_Curvature) {
		return DioVariableType::Float32;
	}
	else if (dioDataType == DioDataType::Array_Depth) {
		return DioVariableType::Uint16;
	}
	else if (dioDataType == DioDataType::Array_Rgb) {
		return DioVariableType::Uint8;
	}
	else if (dioDataType == DioDataType::Array_Intensity) {
		return DioVariableType::Uint8;
	}
	else if (dioDataType == DioDataType::Array_Segmentation) {
		return DioVariableType::Uint8;
	}
	else if (dioDataType == 0) {
		return DioVariableType::Unset;
	}
	else {
		return DioVariableType::Unset;
	}
}


inline bool hasDioDataType(DioDataTypes dioDataTypes, DioDataType dioDataType)
{
	if (dioDataTypes & (DioDataTypes)dioDataType) {
		return true;
	}
	else {
		return false;
	}
}


inline std::vector<DioDataType> getAllDioDataTypes()
{
	std::vector<DioDataType> dioDataTypes;
	dioDataTypes.push_back(DioDataType::Array_Xyz);
	dioDataTypes.push_back(DioDataType::Array_NormalXyz_Curvature);
	dioDataTypes.push_back(DioDataType::Array_Depth);
	dioDataTypes.push_back(DioDataType::Array_Rgb);
	dioDataTypes.push_back(DioDataType::Array_Intensity);
	dioDataTypes.push_back(DioDataType::Array_Segmentation);
	return dioDataTypes;
}


inline std::uint8_t dioDataTypeCount(DioDataTypes dioDataTypes)
{
	std::uint8_t count = 0;
	for (DioDataType type : getAllDioDataTypes()) {
		if (dioDataTypes & (DioDataTypes)type) {
			count++;
		}
	}
	return count;
}


inline UString dioDataTypesToString(DioDataTypes dioDataTypes)
{
	std::vector<DioDataType> allDioDataTypes = getAllDioDataTypes();

	UString s = "[ ";
	bool first = true;
	for (auto it : allDioDataTypes) {
		if (hasDioDataType(dioDataTypes, it) == true) {
			if (first == true) {
				first = false;
			} else {
				s += ", ";
			}
			s += dioDataTypeToString(it);
		}
	}
	if (first == true) {
		s += "NONE";
	}
	s += " ]";
	return s;
}


inline std::ostream& operator << (std::ostream& ost, const DioDataType& dioDataType)
{
	return ost << dioDataTypeToString(dioDataType);
}


} // End of namespace

#endif // End header guard

