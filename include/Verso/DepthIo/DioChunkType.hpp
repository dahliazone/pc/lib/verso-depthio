#ifndef VERSO_DEPTHIO_DIOCHUNKTYPE_HPP
#define VERSO_DEPTHIO_DIOCHUNKTYPE_HPP

#include <Verso/System/UString.hpp>
#include <stdint.h>
#include <vector>

namespace Verso {


enum class DioChunkType : uint8_t
{
	FRAME = 1,
	UNSET = 255
};

inline UString dioChunkTypeToString(DioChunkType type)
{
	if (type == DioChunkType::FRAME)
		return "frame";
	else if (type == DioChunkType::UNSET)
		return "unset";
	else
		return "UNKNOWN";
}


} // End of namespace

#endif // End header guard

