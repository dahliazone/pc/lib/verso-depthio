#ifndef VERSO_DEPTHIO_TILTSTATERAWKINECT_HPP
#define VERSO_DEPTHIO_TILTSTATERAWKINECT_HPP

#include <Verso/verso-depthio-common.hpp>
#include <cstdint>

namespace Verso {


struct VERSO_DEPTHIO_API TiltStateRawKinect
{
public: // static
	static const double COUNTS_PER_G; /*!< Counts per G (=819) for accelerometer Kionix KXSD9 Series accelerometers found in Kinects.
										   See http://www.kionix.com/sites/default/files/KXSD9%20Product%20Brief.pdf */
	static const double GRAVITY_ACCELERATION; /*!< Gravity on earth, 9.80665 m/s^2 */

public: // member variables
	std::int16_t accelerometerX; /*!< Raw Kinect accelerometer data for X-axis. */
	std::int16_t accelerometerY; /*!< Raw Kinect accelerometer data for Y-axis. */
	std::int16_t accelerometerZ; /*!< Raw Kinect accelerometer data for Z-axis. */

public:
	/**
	 * \brief Constructor.
	 */
	TiltStateRawKinect(int16_t accelerometerX = 0, int16_t accelerometerY = 0, int16_t accelerometerZ = 0);

	/**
	 * \brief Setter.
	 */
	void set(int16_t accelerometerX, int16_t accelerometerY, int16_t accelerometerZ);

	/**
	 * \brief Get the axis-based gravity adjusted accelerometer state. See COUNTS_PER_G.
	 */
	void getGravityAdjusted(double& x, double& y, double& z);

	/**
	 * @return true if current and given TiltStateRawKinect objects are equal.
	 */
	bool equals(const TiltStateRawKinect& obj) const;
};


} // End of namespace

#endif // End header guard

