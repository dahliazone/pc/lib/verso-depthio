
#include <iostream>
#include <fstream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>


int main (int argc, char** argv)
{
  std::string line;
  std::ifstream inputFile ("2014-05-03-1937-moptim_dataa.pwpmiitti6.290-15k.kinect.txt");
  if (inputFile.is_open()) {
    int frameNumber = 0;
    int firstTimestamp = -1;
    Frame frame = null;
    while (std::getline(inputFile, line)) {
      // process line
      //std::cout << line << '\n';
      // do something with each line:
      if (line.length() > 0 && line.charAt(0) == '=') {      
        String[] frameInfo = line.split("==");
        int timestamp = int(frameInfo[2]);
        if (firstTimestamp == -1) {
          firstTimestamp = timestamp;
        }
        // Update previous frame's endMillis
        if (frame != null)
          frame.setEndMillis(timestamp - firstTimestamp - 1);
        frame = new Frame(timestamp - firstTimestamp, frameNumber);
        println("Frame "+frame.mFrameNumber+": "+frame.mMillis);
        mFrames.add(frame);
        frameNumber++;
        mPlayLength = frame.mMillis;
      }
      else {
        String[] points = line.split(";");
        for (int j=0; j<points.length; j++) {
          String[] coords = points[j].split(",");
          if (coords.length == 3) {
            //print(coords[0]+":"+coords[1]+":"+coords[2]+"|");
            frame.addPoint(new PVector(float(coords[0]), float(coords[1]), float(coords[2])));
          }
        }
      }
    }
    inputFile.close();
  }

  else  {
    std::cout << "Unable to open file"; 
    exit(1);
  }
  exit(0);



  //pcl::PointCloud<pcl::PointXYZ> cloud;
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);

  // Fill in the cloud data
  cloud->width    = 50;
  cloud->height   = 50;
  cloud->is_dense = false;
  cloud->points.resize (cloud->width * cloud->height);

  for (size_t i = 0; i < cloud->points.size (); ++i)
  {
    cloud->points[i].x = 1024 * rand () / (RAND_MAX + 1.0f);
    cloud->points[i].y = 1024 * rand () / (RAND_MAX + 1.0f);
    cloud->points[i].z = 1024 * rand () / (RAND_MAX + 1.0f);
  }

  // Calculate normals
// Create the normal estimation class, and pass the input dataset to it
  pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
  ne.setInputCloud (cloud);

  // Create an empty kdtree representation, and pass it to the normal estimation object.
  // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
  ne.setSearchMethod (tree);

  // Output datasets
  pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

  // Use all neighbors in a sphere of radius 3cm
  ne.setRadiusSearch (0.03);

  // Compute the features
  ne.compute (*cloud_normals);

  //gateway smt 05770199

  //PointCloud<PointNormal>::Ptr doncloud (new pcl::PointCloud<PointNormal>);
  //copyPointCloud<PointXYZ, PointNormal>(*cloud, *doncloud);

  // cloud_normals->points.size () should have the same size as the input cloud->points.size ()*
  //pcl::io::savePCDFileASCII ("test_pcd.pcd", *cloud);
  std::ofstream outputFile;
  outputFile.open ("test_pcd.nkinect.txt");
  outputFile << "==NEWFRAME==0==\n";
  for (size_t i = 0; i < cloud->points.size (); ++i) {
    std::cerr << "    " << cloud->points[i].x << " " << cloud->points[i].y << " " << cloud->points[i].z << std::endl;
    outputFile << cloud->points[i].x << "," << cloud->points[i].y << "," << cloud->points[i].z << ";";
    outputFile << cloud_normals->points[i].data_c[1] << "," << cloud_normals->points[i].data_c[2] << "," << cloud_normals->points[i].data_c[3] << ";";
  }
  outputFile << "\n";
  outputFile.close();
  std::cerr << "Saved "<<cloud->points.size()<<" data points and "<<cloud_normals->points.size()<<" normals to test_pcd.pcd." << std::endl;

  return (0);
}

