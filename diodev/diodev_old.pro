! include( ../common.pri ) {
	error("Couldn't find the common.pri file!")
}

TEMPLATE = app
SOURCES += main.cpp
CONFIG -= qt core gui

INCLUDEPATH += . ../include
INCLUDEPATH += ../lib/snappy/
INCLUDEPATH += ../lib/Trieng/include
INCLUDEPATH += ../lib/Trieng/lib/utf8-cpp/source/

#LIBS += -L"../foo/$$BUILD_DIR" -lfoo
LIBS += -L"../$$BUILD_DIR" -ldepthio
LIBS += -L../lib/snappy/.libs/ -lsnappy

# Will build the final executable in the main project directory.
DESTDIR = "$$OUT_PWD/../$$BUILD_DIR"
TARGET = dio-dev

linux-* {
	QMAKE_CXXFLAGS += -std=c++17
}

macx {
	QMAKE_CXXFLAGS += -std=c++17

	INCLUDEPATH += /usr/local/include
	#CONFIG -= x86_64
	#CONFIG += x86
	CONFIG -= app_bundle

	LIBDEPTHIO = libdepthio.1.dylib
	QMAKE_POST_LINK += install_name_tool -change "$$LIBDEPTHIO" "@executable_path/$$LIBDEPTHIO" "$$DESTDIR/$$TARGET"

	LIBSNAPPY = libsnappy.1.dylib
	QMAKE_POST_LINK += && cp -f "../lib/snappy/.libs/$$LIBSNAPPY" "$$DESTDIR/"
	QMAKE_POST_LINK += && install_name_tool -change "/usr/local/lib/$$LIBSNAPPY" "@executable_path/$$LIBSNAPPY" "$$DESTDIR/$$TARGET"

	QMAKE_POST_LINK += && cp -rf "../data" "$$DESTDIR/"


	#QMAKE_POST_LINK += cp -f "../foo/$$BUILD_DIR/$$LIBFILE" "$$DESTDIR/$${TARGET}.app/Contents/MacOS/"
	##QMAKE_POST_LINK += && install_name_tool -change "$$LIBFILE" "@executable_path/$$LIBFILE" "$$DESTDIR/$${TARGET}.app/Contents/MacOS/$$TARGET"
}

