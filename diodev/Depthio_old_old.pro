
SOURCES += \
	src/main.cpp \
    src/DepthioPlaintext.cpp \
    src/DepthioBinary.cpp

INCLUDEPATH += include/
INCLUDEPATH += lib/snappy/
INCLUDEPATH += lib/Trieng/include
INCLUDEPATH += lib/Trieng/lib/utf8-cpp/source
CONFIG += link_pkgconfig
CONFIG -= qt core gui
TEMPLATE = app

debug {
	DEFINES += DEBUG
}

# 32 bit
#linux-g++-32 {
#	LIBS += -L$${PWD}/lib/Trieng/lib/BASS/linux
#}

# 64 bit
linux-g++ {
}

linux-* {
	QMAKE_CXXFLAGS += -std=c++17
	LIBS += -L$${PWD}/lib/snappy/.libs/
	LIBS += -lsnappy

	MOC_DIR = "$$OUT_PWD\Linux64"
	OBJECTS_DIR = "$$MOC_DIR"
	RCC_DIR = "$$MOC_DIR"
	UI_DIR = "$$MOC_DIR"
}

macx {
	#CONFIG -= x86_64
	#CONFIG += x86
	#CONFIG -= app_bundle
	QMAKE_CXXFLAGS += -std=c++17
	INCLUDEPATH += /usr/local/include

	LIBS += -L$${PWD}/lib/snappy/.libs/
	LIBS += -lsnappy
}

HEADERS += \
	lib/snappy/snappy.h \
    lib/Trieng/include/UString.hpp \
    lib/Trieng/include/assert.hpp \
    include/PointCloud.hpp \
    include/DepthioPlaintext.hpp \
    include/DepthioBinary.hpp


OTHER_FILES += \
	data/test.280-5k.kinect.txt \
    data/onerow.txt \
    data/test.txt \
    data/tworows.txt \
    data/onerowshort.txt \
    test/tests.pri

SUBDIRS += \
    test/tests.pro


