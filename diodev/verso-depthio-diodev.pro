! include( ../verso-depthio-common.pri ) {
	error("Couldn't find the verso-depthio-common.pri file!")
}

TEMPLATE = app
CONFIG -= qt core gui
# Remove this when the app will be graphical!
CONFIG += console


# Includes
INCLUDEPATH += $$HEADERPATH


# Will build the final executable under the main project directory (builds/<platform>/).
DESTDIR = "$${BUILD_DIR}"
TARGET = libverso-depthio-diodev


# 64 bit
linux-* {
	# First command so && work for the every next ones
	QMAKE_POST_LINK += echo "Post-link process..."

	# Verso-base
	LIBS += -L"$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}" -lverso-base
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}/*.so*" "$$DESTDIR/"

	# Verso-depthio
	LIBS += -L"$${BUILD_DIR}" -lverso-depthio

	# Snappy
	LIBS += -L$${LIBPATH}/snappy/.libs/ -lsnappy
	QMAKE_POST_LINK += && cp -f "$${LIBPATH}/snappy/.libs/libsnappy.so*" "$$DESTDIR/"

	# Scripts
	QMAKE_POST_LINK += && cp -rf "$$PWD/libverso-depthio-diodev.sh" "$$DESTDIR/"

	# Data
	QMAKE_POST_LINK += && cp -rf "$${PWD}/../data" "$$DESTDIR/"
}

macx {
	#CONFIG -= x86_64
	#CONFIG += x86
	CONFIG -= app_bundle

	# Verso-base
	LIBS += -L"$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}" -lverso-base
	QMAKE_POST_LINK += && cp -rf "$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}/*.so" "$$DESTDIR/"
	# TODO: not using install_name_tool

	# Verso-depthio
	LIBS += -L"$${BUILD_DIR}" -lverso-depthio
	# TODO: not using install_name_tool

	# Snappy
	LIBS += -L$${LIBPATH}/snappy/.libs/ -lsnappy
	LIBSNAPPY = libsnappy.1.dylib
	QMAKE_POST_LINK += && cp -f "$${LIBPATH}/snappy/.libs/$$LIBSNAPPY" "$$DESTDIR/"
	QMAKE_POST_LINK += && install_name_tool -change "/usr/local/lib/$$LIBSNAPPY" "@executable_path/$$LIBSNAPPY" "$$DESTDIR/$$TARGET"

	#LIBVERSODEPTHIO = libverso-depthio.1.dylib
	#QMAKE_POST_LINK += install_name_tool -change "$$LIBVERSODEPTHIO" "@executable_path/$$LIBVERSODEPTHIO" "$$DESTDIR/$$TARGET"

#	LIBSNAPPY = libsnappy.1.dylib
#	QMAKE_POST_LINK += && cp -f "$${LIBPATH}/snappy/.libs/$$LIBSNAPPY" "$$DESTDIR/"
#	QMAKE_POST_LINK += && install_name_tool -change "/usr/local/lib/$$LIBSNAPPY" "@executable_path/$$LIBSNAPPY" "$$DESTDIR/$$TARGET"

	# TODO: not up-to-date
#	QMAKE_POST_LINK += && cp -rf "libverso-depthio-diodev.sh" "$$DESTDIR/"
#	QMAKE_POST_LINK += && cp -rf "../data" "$$DESTDIR/"

	#QMAKE_POST_LINK += cp -f "../foo/$$BUILD_DIR/$$LIBFILE" "$$DESTDIR/$${TARGET}.app/Contents/MacOS/"
	##QMAKE_POST_LINK += && install_name_tool -change "$$LIBFILE" "@executable_path/$$LIBFILE" "$$DESTDIR/$${TARGET}.app/Contents/MacOS/$$TARGET"
}

win32:CONFIG(debug, debug|release) {
	# Debug output on console
	CONFIG += console
}

win* {
	# Verso-base
	LIBS += -L"$${LIBPATH}/verso-base/builds/$${BUILD_SUBDIR}" -lverso-base

	# Verso-depthio
	LIBS += -L"$${BUILD_DIR}" -lverso-depthio

	# SDL2 winmain
	LIBS += -lSDL2main -lSDL2
}


DEPENDPATH += \
	$${PWD}


SOURCES += \
	diodev-main.cpp

