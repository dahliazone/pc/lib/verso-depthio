#!/bin/bash

echo "verso-depthio: Fetching externals from repositories..."

if [ ! -d ../snappy ]; then
	git clone https://gitlab.com/dahliazone/pc/extlib/snappy.git ../snappy
else
	pushd ../snappy
	git pull --rebase
	popd
fi

if [ ! -d ../freenect ]; then
	git clone https://gitlab.com/dahliazone/pc/extlib/freenect.git ../freenect
else
	pushd ../freenect
	git pull --rebase
	popd
fi

if [ ! -d ../verso-3d ]; then
        git clone https://gitlab.com/dahliazone/pc/lib/verso-3d.git ../verso-3d
else
        pushd ../verso-3d
        git pull --rebase
        popd
fi

echo "verso-3d: Resursing into..."
pushd ../verso-3d
./fetch_parent_externals.sh
popd

echo "verso-depthio: All done"

