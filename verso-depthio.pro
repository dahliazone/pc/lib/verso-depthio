TEMPLATE = subdirs
win* {
		TEMPLATE = subdirs
}
CONFIG += ordered

libverso-3d.subdir = ../verso-3d
libverso-3d.target = libverso-3d-dep

libverso-depthio.file = src/verso-depthio-src.pro
libverso-depthio.target = libverso-depthio-dep
libverso-depthio.depends = libverso-3d-dep

tests_libverso-depthio.file = test/verso-depthio-test.pro
tests_libverso-depthio.target = tests_libverso-depthio-dep
tests_libverso-depthio.depends = libverso-depthio-dep

examples_libverso-depthio.file = examples/verso-depthio-examples.pro
examples_libverso-depthio.depends = tests_libverso-depthio-dep

camcorder_libverso-depthio.file = camcorder/verso-depthio-camcorder.pro
camcorder_libverso-depthio.depends = tests_libverso-depthio-dep

diodev_libverso-depthio.file = diodev/verso-depthio-diodev.pro
diodev_libverso-depthio.depends = tests_libverso-depthio-dep


SUBDIRS = libverso-3d libverso-depthio tests_libverso-depthio examples_libverso-depthio camcorder_libverso-depthio diodev_libverso-depthio

