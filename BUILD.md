Verso-DepthIO library
=====================

Repository: https://gitlab.com/dahliazone/pc/lib/verso-depthio

You will need qmake installed. New version (5.x) of Qt SDK is recommended.

Required:
```
sudo apt-get install build-essential libglewmx-dev libsdl2-dev libsdl2-image-dev libsdl2-ttf-dev libboost1.55-dev
sudo apt-get install freeglut3-dev pkg-config libxmu-dev libxi-dev libusb-1.0-0-dev
sudo apt-get install subversion git
```

Recommended:
```
sudo apt-get install valgrind
```

Compiling
=========

Clone the repository
```
git clone https://gitlab.com/dahliazone/pc/lib/verso-depthio.git
cd verso-depthio
```

Install other dependencies
```
./fetch_externals.sh
```
(this will also compile them when needed)

Compile the library and test programs
```
qmake
make -j5
```

Running
=======
Install udev-rules from `freenect/platform/linux` or run as root.


```
cd builds/linux-gcc64-debug
./ libverso-depthio-camcorder.sh
```
