#Includes common configuration for all subdirectory .pro files.
INCLUDEPATH += . .. ./include
WARNINGS += -Wall


debug {
		DEFINES += DEBUG
}


# Linux g++ 32-bit
linux-g++-32:CONFIG(debug, debug|release) {
	BUILD_DIR = "builds/linux-gcc32-debug"
	DESTDIR = "$$OUT_PWD/$$BUILD_DIR"
	UI_DIR = "$$DESTDIR/uics"
	MOC_DIR = "$$DESTDIR/mocs"
	OBJECTS_DIR = "$$DESTDIR/objs"
	QMAKE_CXXFLAGS += -std=c++17
}

linux-g++-32:CONFIG(release, debug|release) {
	BUILD_DIR = "builds/linux-gcc32-release"
	DESTDIR = "$$OUT_PWD/$$BUILD_DIR"
	UI_DIR = "$$DESTDIR/uics"
	MOC_DIR = "$$DESTDIR/mocs"
	OBJECTS_DIR = "$$DESTDIR/objs"
	QMAKE_CXXFLAGS += -std=c++17
}


# Linux g++ 64-bit
linux-g++:CONFIG(debug, debug|release) {
	BUILD_DIR = "builds/linux-gcc64-debug"
	DESTDIR = "$$OUT_PWD/$$BUILD_DIR"
	UI_DIR = "$$DESTDIR/uics"
	MOC_DIR = "$$DESTDIR/mocs"
	OBJECTS_DIR = "$$DESTDIR/objs"
	QMAKE_CXXFLAGS += -std=c++17
}

linux-g++:CONFIG(release, debug|release) {
	BUILD_DIR = "builds/linux-gcc64-release"
	DESTDIR = "$$OUT_PWD/$$BUILD_DIR"
	UI_DIR = "$$DESTDIR/uics"
	MOC_DIR = "$$DESTDIR/mocs"
	OBJECTS_DIR = "$$DESTDIR/objs"
	QMAKE_CXXFLAGS += -std=c++17
}


# Mac OS X
macx:CONFIG(debug, debug|release) {
	BUILD_DIR = "builds/macosx-debug"
	DESTDIR = "$$OUT_PWD/$$BUILD_DIR"
	UI_DIR = "$$DESTDIR/uics"
	MOC_DIR = "$$DESTDIR/mocs"
	OBJECTS_DIR = "$$DESTDIR/objs"
	QMAKE_CXXFLAGS += -std=c++17
}

macx:CONFIG(release, debug|release) {
	BUILD_DIR = "builds/macosx-release"
	DESTDIR = "$$OUT_PWD/$$BUILD_DIR"
	UI_DIR = "$$DESTDIR/uics"
	MOC_DIR = "$$DESTDIR/mocs"
	OBJECTS_DIR = "$$DESTDIR/objs"
	QMAKE_CXXFLAGS += -std=c++17
}

# Win32
win32:CONFIG(debug, debug|release) {
	LIBS += -L\"$$DAHLIA_VC\lib\Win32\Debug\"
	DLL_DIR = "$$DAHLIA_VC\dll\Win32\Debug"

	BUILD_DIR = "builds\win32-debug"
	DESTDIR = "$$OUT_PWD\$$BUILD_DIR"
	UI_DIR = "$$DESTDIR\uics"
	MOC_DIR = "$$DESTDIR\mocs"
	OBJECTS_DIR = "$$DESTDIR\objs"
}

win32:CONFIG(release, debug|release) {
	LIBS += -L\"$$DAHLIA_VC\lib\Win32\Release\"
	DLL_DIR = "$$DAHLIA_VC\dll\Win32\Release"

	BUILD_DIR = "builds\win32-debug"
	DESTDIR = "$$OUT_PWD\$$BUILD_DIR"
	DESTDIR = "$$OUT_PWD\builds\win32-release"
	UI_DIR = "$$DESTDIR\uics"
	MOC_DIR = "$$DESTDIR\mocs"
	OBJECTS_DIR = "$$DESTDIR\objs"
}

# Win64
win64:CONFIG(debug, debug|release) {
	LIBS += -L\"$$DAHLIA_VC\lib\x64\Debug\"
	DLL_DIR = "$$DAHLIA_VC\dll\x64\Debug"

	BUILD_DIR = "builds/win64-debug"
	DESTDIR = "$$OUT_PWD\builds\win64-debug"
	UI_DIR = "$$DESTDIR\uics"
	MOC_DIR = "$$DESTDIR\mocs"
	OBJECTS_DIR = "$$DESTDIR/objs"
}

win64:CONFIG(release, debug|release) {
	LIBS += -L\"$$DAHLIA_VC\lib\x64\Release\"
	DLL_DIR = "$$DAHLIA_VC\dll\x64\Release"

	BUILD_DIR = "builds/win64-release"
	DESTDIR = "$$OUT_PWD\$$BUILD_DIR"
	UI_DIR = "$$DESTDIR\uics"
	MOC_DIR = "$$DESTDIR\mocs"
	OBJECTS_DIR = "$$DESTDIR/objs"
}

